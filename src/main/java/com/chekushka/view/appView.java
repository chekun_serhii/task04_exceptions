package com.chekushka.view;

import com.chekushka.controller.Controller;
import com.chekushka.model.exceptions.EmailException;

import java.util.Scanner;

public class appView {
    private static Scanner input = new Scanner(System.in);
    private Controller controller = new Controller();

    public final void show() throws EmailException {
        while (true) {
            System.out.println("Select an option:\n"
                    + "1. Log In\n"
                    + "2. Create email\n"
                    + "3. Your emails \n"
                    + "4. Exit\n");
            System.out.println("Enter here:");
            int choice = input.nextInt();
            try {
                switch (choice) {
                    case (1):
                        controller.logIn();
                        break;
                    case (2):
                        controller.createMail();
                        break;
                    case (3):
                        controller.printMail();
                        break;
                    case (4):
                        System.exit(0);
                        break;
                }
            } catch (Exception NoOptionException) {
                System.out.println("There is no option with this number!");
            }

        }
    }
}

