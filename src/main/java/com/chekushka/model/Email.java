package com.chekushka.model;

public class Email {
    private String author;
    private String subject;
    private String content;

    public Email(String author, String subject, String content) {
        this.author = author;
        this.subject = subject;
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
