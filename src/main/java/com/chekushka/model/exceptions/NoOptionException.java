package com.chekushka.model.exceptions;

public class NoOptionException extends Exception{
    public NoOptionException(String message){
        super(message);
    }
}
