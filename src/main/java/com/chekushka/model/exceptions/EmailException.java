package com.chekushka.model.exceptions;

public class EmailException extends Exception {
    private String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public EmailException(String message, String emailAddress) {
        super(message);
        this.emailAddress = emailAddress;
    }
}
