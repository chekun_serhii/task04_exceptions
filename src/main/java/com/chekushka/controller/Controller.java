package com.chekushka.controller;

import com.chekushka.model.Email;
import com.chekushka.model.exceptions.EmailException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {
    private List<Email> emailList = new ArrayList<>();
    private Scanner input = new Scanner(System.in);
    private String emailAddress;

    public final void logIn() throws EmailException {
        System.out.println("Enter your email:");
        emailAddress = input.next();
        if (!emailAddress.contains("@")) {
            throw new EmailException("This is not correct email", emailAddress);
        } else {
            System.out.println("Log in successfully!");
        }
    }

    public final void createMail() {
        System.out.println("Enter the subject:");
        String subject = input.next();
        System.out.println("Enter the subject:");
        String content = input.next();
        Email newMail = new Email(emailAddress, subject, content);
        try {
            emailList.add(newMail);
        } catch (Exception NullPointerException) {
            System.out.println("Oops. Something went wrong. Try again!");
        }
    }

    public final void printMail() {
        for (int i = 0; i < emailList.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + emailList.get(i));
        }
    }
}
