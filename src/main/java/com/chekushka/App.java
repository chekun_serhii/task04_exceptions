package com.chekushka;

import com.chekushka.model.exceptions.EmailException;
import com.chekushka.view.appView;

public class App {
    public static void main(String[] args) throws EmailException {
        appView start = new appView();
        start.show();
    }
}
